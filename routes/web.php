<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/detailproduct/{id}', 'CDetailproduct@index'); //simpan
Route::get('/detailproduct/hapusComment/{id_product}/{id_comments}', 'CDetailproduct@hapusComment'); //delete
Route::post('/detailproduct/simpanComment', 'CDetailproduct@simpanComment'); //hapus
Route::get('/detailproduct/wishlist/{id_product}', 'CDetailproduct@wishlist'); //hapus

// Route::get('/login', 'CLogin@index');
// Route::get('/login_verifikasi', 'CLogin@verifikasi');

Auth::routes('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
