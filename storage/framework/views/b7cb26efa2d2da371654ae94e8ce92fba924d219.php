<?php $__env->startSection('content'); ?>

	
	<br>
	<!--single product wrapper start-->
	<div class="single_product_wrapper">
			<div class="container">
					<div class="row">
							<div class="col-lg-5 col-md-6">
									<div class="product_gallery">
											<div class="tab-content produc_thumb_conatainer">
												<?php
													$i=1;
												?>
												<?php $__currentLoopData = $Datafotoproduct->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if($key->status=='ACTIVE'): ?>
														<div class="tab-pane fade show active" id="<?php echo e($key->id_foto_product); ?>" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="<?php echo e(url('products/600x600/'.$key->gambar)); ?>" alt=""></a>
															</div>
														</div>
													<?php else: ?>
														<div class="tab-pane fade" id="<?php echo e($key->id_foto_product); ?>" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="<?php echo e(url('products/600x600/'.$key->gambar)); ?>" alt=""></a>
															</div>
														</div>
													<?php endif; ?>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</div>
											<div class="product_thumb_button">
													<ul class="nav product_d_button" role="tablist">
															<?php
																$i=1;
															?>
															<?php $__currentLoopData = $Datafotoproduct->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($key->status=='ACTIVE'): ?>
																<li >
																	<a class="active" data-toggle="tab" href="#<?php echo e($key->id_foto_product); ?>" role="tab" aria-controls="<?php echo e($key->id_foto_product); ?>" aria-selected="true"><img src="<?php echo e(url('products/118x118/'.$key->gambar)); ?>" alt=""></a>
																</li>
																<?php else: ?>
																<li >
																	<a data-toggle="tab" href="#<?php echo e($key->id_foto_product); ?>" role="tab" aria-controls="<?php echo e($key->id_foto_product); ?>" aria-selected="false"><img src="<?php echo e(url('products/118x118/'.$key->gambar)); ?>" alt=""></a>
																</li>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

													</ul>
											</div>
									</div>
							</div>
							<div class="col-lg-7 col-md-6">
									<div class="product_details">
											<h3><?php echo e($Dataproduct->nama_produk); ?></h3>
											<div class="countdown_product_price">
												<?php if($Datadiscount==null): ?>
													<span class="current_price"> Rp. <?php echo e(number_format($Dataproduct->harga,0)); ?></span>
												<?php else: ?>
													<span class="current_price"> Rp. <?php echo e(number_format($Datadiscount['hasil'],0)); ?></span>
													<span class="old_price"> Rp. <?php echo e(number_format($Dataproduct->harga,0)); ?></span>

												<?php endif; ?>

											</div>
											<div class="product_ratting">
													<ul>
															<li><a href="#"><i class="ion-star"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
													</ul>
													<ul>
															<li><a href="#">1 Review</a></li>
													</ul>
											</div>
										 <div class="product_description">
												 <p><?php echo e($Dataproduct->deskripsi); ?></p>
										 </div>
											<div class="product_details_action">
													<h3>Available Options</h3>
													<div class="product_stock">
															<label>Quantity</label>
															<input min="0" max="100" type="number">
													</div>
													<div class="product_action_link">
															<ul>
																	<li class="product_cart"><a href="#" title="Add to Cart">Langsung beli</a></li>
																	<li class="product_cart"><a href="#" title="Add to Cart">Add to Cart</a></li>
																	<li class="add_links"><a href="#" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> Add to wishlist</a></li>
																	
															</ul>
													</div>
													<div class="social_sharing">
															<span>Share</span>
															<ul>
																	<li><a href="#" class="bg-facebook" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i> Share</a></li>
																	<li><a href="#" class="bg-Tweet" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i> Tweet</a></li>
																	<li>
																		<a href="<?php echo e(url('detailproduct/'.$Dataproduct->id_product)); ?>"
																		onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
																		class="bg-google" data-toggle="tooltip" title="google-plus">
																			<i class="fa fa-google-plus"></i> Google+
																		</a>
																	</li>
																	<li><a href="#" class="bg-pinterest" data-toggle="tooltip" title="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a></li>
															</ul>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--single product wrapper end-->

	<!--product info start-->
	<div class="product_d_info">
			<div class="container">
					<div class="row">
							<div class="col-12">
									<div class="product_d_inner">
											<div class="product_info_button">
													<ul class="nav" role="tablist">
															<li >
																	<a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">More info</a>
															</li>
															
															<li>
																 <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">comments</a>
															</li>
													</ul>
											</div>
											<div class="tab-content">
													<div class="tab-pane fade show active" id="info" role="tabpanel" >
															<div class="product_info_content">
																	<p><?php echo e($Dataproduct->more_info); ?></p>
															</div>
													</div>
													<div class="tab-pane fade" id="sheet" role="tabpanel" >
															<div class="product_d_table">
																 <form action="#">
																			<table>
																					<tbody>
																							<tr>
																									<td class="first_child">Compositions</td>
																									<td>Polyester</td>
																							</tr>
																							<tr>
																									<td class="first_child">Styles</td>
																									<td>Girly</td>
																							</tr>
																							<tr>
																									<td class="first_child">Properties</td>
																									<td>Short Dress</td>
																							</tr>
																					</tbody>
																			</table>
																	</form>
															</div>
															<div class="product_info_content">
																	<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
															</div>
													</div>
													<div class="tab-pane fade" id="reviews" role="tabpanel" >
															<div class="product_review_form">
																	<?php echo e(Form::open(array('url' => 'detailproduct'))); ?>

																		<h2>Add a comment </h2>
																		<p>Your email address will not be published. Required fields are marked </p>
																		<?php if(Route::has('login')): ?>
                                      <?php if(auth()->guard()->check()): ?>
																				<div class="row">
																					<div class="col-12">
																						<label for="review_comment">Your review </label>
																						<textarea name="comment" id="review_comment" ><?php echo e(old('comment')); ?></textarea>
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label disabled for="author">Name</label>
																						<input disabled id="author" value="<?php echo e(Auth::user()->name); ?>" type="text">
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label for="email">Email </label>
																						<input disabled id="email" value="<?php echo e(Auth::user()->email); ?>" type="text">
																					</div>
																				</div>
																				<input hidden type="text" name="id_product" value="<?php echo e($Dataproduct->id_product); ?>">
																				<?php echo e(csrf_field()); ?>

																				<button type="submit">Submit</button>
                                      <?php else: ?>
																				<a href="<?php echo e(url('auth/google')); ?>" title="Google+" class="col-md-12 btn btn-googleplus btn-lg">
																					<i class="fa fa-google-plus fa-fw"></i> Login and Regis With Google
																				</a>
                                      <?php endif; ?>
                                    <?php endif; ?>
																 <?php echo e(Form::close()); ?>

															</div>
															<?php if(count($Datacomment)>0): ?>
															
															<div class="comments_box">
                                <h3><?php echo e(count($Datacomment)); ?> comments	</h3>
																<?php $__currentLoopData = $Datacomment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<div class="comment_list">
																	<div class="comment_content">
																		<div class="comment_meta">
																			<div class="comment_title">
																				<h5><a href="#"><?php echo e($key->name); ?></a></h5>
																				<span><?php echo e(date('F d, Y', strtotime($key->created_at_cmt))); ?> at <?php echo e(date('h:i a', strtotime($key->created_at_cmt))); ?></span>
																			</div>
																		</div>
																		<p><?php echo e($key->comment); ?></p>
																	</div>
																</div>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </div>
															
															<?php endif; ?>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--product info end-->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/layout_1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>