<?php $__env->startSection('content'); ?>

	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
			<div class="container">
					<div class="breadcrumbs_inner">
							<div class="row">
									<div class="col-12">
											<div class="breadcrumb_content">
													<h3>product details</h3>
													<ul>
															<li><a href="index.html">home</a></li>
															<li><i class="fa fa-angle-right"></i></li>
															<li>product details</li>
													</ul>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--breadcrumbs area end-->

	<!--single product wrapper start-->
	<div class="single_product_wrapper">
			<div class="container">
					<div class="row">
							<div class="col-lg-5 col-md-6">
									<div class="product_gallery">
											<div class="tab-content produc_thumb_conatainer">
													<div class="tab-pane fade show active" id="p_tab1" role="tabpanel" >
															<div class="modal_img">
																	<a href="#"><img src="<?php echo e(url('cigar/assets/img/product/bigimg1.jpg')); ?>" alt=""></a>
															</div>
													</div>
													<div class="tab-pane fade" id="p_tab2" role="tabpanel">
															<div class="modal_img">
																	<a href="#"><img src="<?php echo e(url('cigar/assets/img/product/bigimg2.jpg')); ?>" alt=""></a>
															</div>
													</div>
													<div class="tab-pane fade" id="p_tab3" role="tabpanel">
															<div class="modal_img">
																	<a href="#"><img src="<?php echo e(url('cigar/assets/img/product/bigimg3.jpg')); ?>" alt=""></a>
															</div>
													</div>
													<div class="tab-pane fade" id="p_tab4" role="tabpanel">
															<div class="modal_img">
																	<a href="#"><img src="<?php echo e(url('cigar/assets/img/product/bigimg4.jpg')); ?>" alt=""></a>
															</div>
													</div>
											</div>

											<div class="product_thumb_button">
													<ul class="nav product_d_button" role="tablist">
															<li >
																	<a class="active" data-toggle="tab" href="#p_tab1" role="tab" aria-controls="p_tab1" aria-selected="false"><img src="<?php echo e(url('cigar/assets/img/cart/cart10.jpg')); ?>" alt=""></a>
															</li>
															<li>
																	 <a data-toggle="tab" href="#p_tab2" role="tab" aria-controls="p_tab2" aria-selected="false"><img src="<?php echo e(url('cigar/assets/img/cart/cart11.jpg')); ?>" alt=""></a>
															</li>
															<li>
																 <a data-toggle="tab" href="#p_tab3" role="tab" aria-controls="p_tab3" aria-selected="false"><img src="<?php echo e(url('cigar/assets/img/cart/cart9.jpg')); ?>" alt=""></a>
															</li>
															<li>
																 <a data-toggle="tab" href="#p_tab4" role="tab" aria-controls="p_tab4" aria-selected="false"><img src="<?php echo e(url('cigar/assets/img/cart/cart12.jpg')); ?>" alt=""></a>
															</li>
													</ul>
											</div>
									</div>
							</div>
							<div class="col-lg-7 col-md-6">
									<div class="product_details">
											<h3>Nonstick Dishwasher PFOA</h3>
											<div class="product_price">
													<span class="current_price">$23.00</span>
													<span class="old_price">$28.00</span>
											</div>
											<div class="product_ratting">
													<ul>
															<li><a href="#"><i class="ion-star"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
													</ul>
													<ul>
															<li><a href="#">1 Review</a></li>
													</ul>
											</div>
										 <div class="product_description">
												 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis laborum, facilis in provident pariatur, assumenda accusantium asperiores iste corrupti laboriosam quasi eius illum minus aperiam doloribus, distinctio unde? Nam recusandae ipsam repellendus repellat eum nisi obcaecati, doloremque mollitia iste, ab delectus, error quia, quae eligendi!</p>
										 </div>
											<div class="product_details_action">
													<h3>Available Options</h3>
													<div class="product_stock">
															<label>Quantity</label>
															<input min="0" max="100" type="number">
													</div>
													<div class="product_action_link">
															<ul>
																	<li class="product_cart"><a href="#" title="Add to Cart">Add to Cart</a></li>
																	<li class="add_links"><a href="#" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> Add to wishlist</a></li>
																	<li class="add_links"><a href="#" title="Add to Compare"><i class="ion-loop"></i> Add to compare</a></li>
															</ul>
													</div>
													<div class="social_sharing">
															<span>Share</span>
															<ul>
																	<li><a href="#" class="bg-facebook" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i> Share</a></li>
																	<li><a href="#" class="bg-Tweet" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i> Tweet</a></li>
																	<li><a href="#" class="bg-google" data-toggle="tooltip" title="google-plus"><i class="fa fa-google-plus"></i> Google+</a></li>
																	<li><a href="#" class="bg-pinterest" data-toggle="tooltip" title="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a></li>
															</ul>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--single product wrapper end-->

	<!--product info start-->
	<div class="product_d_info">
			<div class="container">
					<div class="row">
							<div class="col-12">
									<div class="product_d_inner">
											<div class="product_info_button">
													<ul class="nav" role="tablist">
															<li >
																	<a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">More info</a>
															</li>
															<li>
																	 <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Data sheet</a>
															</li>
															<li>
																 <a data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>
															</li>
													</ul>
											</div>
											<div class="tab-content">
													<div class="tab-pane fade show active" id="info" role="tabpanel" >
															<div class="product_info_content">
																	<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
															</div>
													</div>

													<div class="tab-pane fade" id="sheet" role="tabpanel" >
															<div class="product_d_table">
																 <form action="#">
																			<table>
																					<tbody>
																							<tr>
																									<td class="first_child">Compositions</td>
																									<td>Polyester</td>
																							</tr>
																							<tr>
																									<td class="first_child">Styles</td>
																									<td>Girly</td>
																							</tr>
																							<tr>
																									<td class="first_child">Properties</td>
																									<td>Short Dress</td>
																							</tr>
																					</tbody>
																			</table>
																	</form>
															</div>
															<div class="product_info_content">
																	<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
															</div>
													</div>
													<div class="tab-pane fade" id="reviews" role="tabpanel" >
															<div class="product_info_content">
																	<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
															</div>
															<div class="product_info_inner">
																	<div class="product_ratting mb-10">
																			<ul>
																					<li><a href="#"><i class="ion-star"></i></a></li>
																					<li><a href="#"><i class="ion-star"></i></a></li>
																					<li><a href="#"><i class="ion-star"></i></a></li>
																					<li><a href="#"><i class="ion-star"></i></a></li>
																					<li><a href="#"><i class="ion-star"></i></a></li>
																			</ul>
																			<strong>Posthemes</strong>
																			<p>09/07/2018</p>
																	</div>
																	<div class="product_demo">
																			<strong>demo</strong>
																			<p>That's OK!</p>
																	</div>
															</div>
															<div class="product_review_form">
																	<form action="#">
																			<h2>Add a review </h2>
																			<p>Your email address will not be published. Required fields are marked </p>
																			<div class="row">
																					<div class="col-12">
																							<label for="review_comment">Your review </label>
																							<textarea name="comment" id="review_comment" ></textarea>
																					</div>
																					<div class="col-lg-6 col-md-6">
																							<label for="author">Name</label>
																							<input id="author"  type="text">

																					</div>
																					<div class="col-lg-6 col-md-6">
																							<label for="email">Email </label>
																							<input id="email"  type="text">
																					</div>
																			</div>
																			<button type="submit">Submit</button>
																	 </form>
															</div>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--product info end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/layout_1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>