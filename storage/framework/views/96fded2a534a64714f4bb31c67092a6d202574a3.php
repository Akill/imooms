<?php $__env->startSection('content'); ?>

	
	<br>
	<!--single product wrapper start-->
	<div class="single_product_wrapper">
			<div class="container">
					<div class="row">
							<div class="col-lg-5 col-md-6">
									<div class="product_gallery">
											<div class="tab-content produc_thumb_conatainer">
												<?php
													$i=1;
												?>
												<?php $__currentLoopData = $Datafotoproduct->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if($key->status=='ACTIVE'): ?>
														<div class="tab-pane fade show active" id="<?php echo e($key->id_foto_product); ?>" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="<?php echo e(url('products/600x600/'.$key->gambar)); ?>" alt=""></a>
															</div>
														</div>
													<?php else: ?>
														<div class="tab-pane fade" id="<?php echo e($key->id_foto_product); ?>" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="<?php echo e(url('products/600x600/'.$key->gambar)); ?>" alt=""></a>
															</div>
														</div>
													<?php endif; ?>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</div>
											<div class="product_thumb_button">
													<ul class="nav product_d_button" role="tablist">
															<?php
																$i=1;
															?>
															<?php $__currentLoopData = $Datafotoproduct->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($key->status=='ACTIVE'): ?>
																<li >
																	<a class="active" data-toggle="tab" href="#<?php echo e($key->id_foto_product); ?>" role="tab" aria-controls="<?php echo e($key->id_foto_product); ?>" aria-selected="true"><img src="<?php echo e(url('products/118x118/'.$key->gambar)); ?>" alt=""></a>
																</li>
																<?php else: ?>
																<li >
																	<a data-toggle="tab" href="#<?php echo e($key->id_foto_product); ?>" role="tab" aria-controls="<?php echo e($key->id_foto_product); ?>" aria-selected="false"><img src="<?php echo e(url('products/118x118/'.$key->gambar)); ?>" alt=""></a>
																</li>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

													</ul>
											</div>
									</div>
							</div>
							<div class="col-lg-7 col-md-6">
									<div class="product_details">
											<h3><?php echo e($Dataproduct->nama_produk); ?></h3>
											<div class="countdown_product_price">
												<?php if($Datadiscount==null): ?>
													<span class="current_price"> Rp. <?php echo e(number_format($Dataproduct->harga,0)); ?></span>
												<?php else: ?>
													<span class="current_price"> Rp. <?php echo e(number_format($Datadiscount['hasil'],0)); ?></span>
													<span class="old_price"> Rp. <?php echo e(number_format($Dataproduct->harga,0)); ?></span>

												<?php endif; ?>

											</div>
											<div class="product_ratting">
													<ul>
														<?php if($Datarating==null): ?>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
														<?php else: ?>
															<?php for($i=0; $i < 5; $i++): ?>
																<?php if($Datarating->rating>$i): ?>
																	<li><a href="#"><i class="ion-ios-star"></i></a></li>
																<?php else: ?>
																	<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
																<?php endif; ?>
															<?php endfor; ?>
														<?php endif; ?>
													</ul>
													<ul>
														<li><a href="#komennya"><?php echo e($Datacomment->total()); ?> Comments</a></li>
													</ul>
											</div>
										 <div class="product_description">
											 	<?php if($Dataproduct->stock>0): ?>
												 	<p>Sisa Produk : <label><?php echo e($Dataproduct->stock); ?></label></p>
												<?php else: ?>
													<p>Sisa Produk : <label style="color:red"><?php echo e($Dataproduct->stock); ?> maaf persediaan habis</label></p>
												<?php endif; ?>
												<p><?php echo e($Dataproduct->deskripsi); ?></p>
										 </div>
											<div class="product_details_action">
													<h3>Available Options</h3>
													<?php if($Dataproduct->stock>0): ?>
													<div class="product_stock">
															<label>Quantity</label>
															<input min="1" max="<?php echo e($Dataproduct->stock); ?>" type="number">
													</div>
													<div class="product_action_link">
															<ul>
																	<li class="product_cart"><a href="#" title="Add to Cart">Langsung beli</a></li>
																	<li class="product_cart"><a href="#" title="Add to Cart">Add to Cart</a></li>
																	<?php if(count($Datawishlist['data'])>0): ?>
																		<?php if($Datawishlist['valid']): ?>
																			<li class="add_links"><a href="<?php echo e(url('detailproduct/wishlist/'.$Dataproduct->id_product)); ?>" title="Add to Wishlist"><i class="ion-ios-heart"></i> <?php echo e(count($Datawishlist['data'])); ?> wishlist</a></li>
																		<?php else: ?>
																			<?php if(\Auth::user()): ?>
																				<li class="add_links"><a href="<?php echo e(url('detailproduct/wishlist/'.$Dataproduct->id_product)); ?>" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> <?php echo e(count($Datawishlist['data'])); ?> wishlist</a></li>
																			<?php else: ?>
																				<li class="add_links"><a href="<?php echo e(url('login')); ?>" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> <?php echo e(count($Datawishlist['data'])); ?> wishlist</a></li>
																			<?php endif; ?>
																		<?php endif; ?>
																	<?php else: ?>
																		<li class="add_links"><a href="<?php echo e(url('detailproduct/wishlist/'.$Dataproduct->id_product)); ?>" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> <?php echo e(count($Datawishlist['data'])); ?> wishlist</a></li>
																	<?php endif; ?>
																	
															</ul>
													</div>
													<?php else: ?>
													<?php endif; ?>
													<div class="social_sharing">
															<span>Share :</span>
															<ul>
																	
																	<li>
																		<a href="<?php echo e(url('detailproduct/'.$Dataproduct->id_product)); ?>"
																		onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
																		class="bg-google" data-toggle="tooltip" title="google">
																			<i class="fa fa-google"> </i> Google
																		</a>
																	</li>
															</ul>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--single product wrapper end-->

	<!--product info start-->
	<div class="product_d_info">
			<div class="container">
					<div class="row">
							<div class="col-12">
									<div class="product_d_inner">
											<div class="product_info_button">
													<ul class="nav" role="tablist">
															<li >
																	<a data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">More info</a>
															</li>
															
															<li>
																 <a class="active" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">comments</a>
															</li>
													</ul>
											</div>
											<div class="tab-content">
													<div class="tab-pane fade" id="info" role="tabpanel" >
															<div class="product_info_content">
																	<p>Tanggal Entry : <b><?php echo e(date('F d, Y', strtotime($Dataproduct->updated_at))); ?> at <?php echo e(date('h:i a', strtotime($Dataproduct->updated_at))); ?></b></p>
																	<br>
																	<p><?php echo e($Dataproduct->more_info); ?></p>
															</div>
													</div>
													<div class="tab-pane fade show active" id="reviews" role="tabpanel" >
															<div class="product_review_form">
																	<?php echo e(Form::open(array('url' => 'detailproduct/simpanComment'))); ?>

																		<h2>Add a comment </h2>
																		<p>Your email address will not be published. Required fields are marked </p>
																		<?php if(Route::has('login')): ?>
                                      <?php if(auth()->guard()->check()): ?>
																				<div class="row">
																					<div class="col-12">
																						<?php if($errors->any()): ?>
																							<label for="review_comment" style="color:red;"><?php echo e($errors->all()[0]); ?></label>
																						<?php endif; ?>
																						<textarea name="comment" id="review_comment" ><?php echo e(old('comment')); ?></textarea>
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label disabled for="author">Name</label>
																						<input disabled id="author" value="<?php echo e(Auth::user()->name); ?>" type="text">
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label for="email">Email </label>
																						<input disabled id="email" value="<?php echo e(Auth::user()->email); ?>" type="text">
																					</div>
																				</div>
																				<input hidden type="text" name="id_product" value="<?php echo e($Dataproduct->id_product); ?>">
																				<?php echo e(csrf_field()); ?>

																				<button type="submit">Submit</button>
                                      <?php else: ?>
																				<a href="<?php echo e(url('auth/google')); ?>" title="Google+" class="col-md-12 btn btn-googleplus btn-lg">
																					<i class="fa fa-google fa-fw"></i> Login and Regis With Google
																				</a>
                                      <?php endif; ?>
                                    <?php endif; ?>
																 <?php echo e(Form::close()); ?>

															</div>
															<?php if(count($Datacomment)>0): ?>
															
															<div class="comments_box">
                                <h3 id="komennya"><?php echo e(count($Datacomment)); ?> - <?php echo e($Datacomment->total()); ?> comments	</h3>
																<?php $__currentLoopData = $Datacomment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<div class="comment_list">
																	<div class="comment_content">
																		<div class="comment_meta">
																			<div class="comment_title">
																				<h5><a href="#"><?php echo e($key->name); ?></a>
																					<?php if( Session::has('flash_message') ): ?>
																						<?php if( Session::get('flash_id_comment')==$key->id_comments ): ?>
																							<label style="color:green; font-size:0.7em;"><?php echo e(Session::get('flash_message')); ?></label>
																						<?php endif; ?>
																					<?php endif; ?>
																				</h5>
																				<span><?php echo e(date('F d, Y', strtotime($key->created_at_cmt))); ?> at <?php echo e(date('h:i a', strtotime($key->created_at_cmt))); ?></span>
																			</div>
																		</div>
																		<p><?php echo e($key->comment); ?></p>
																		<?php if(\Auth::user()): ?>
																			<?php if($key->id==\Auth::user()->id): ?>
																				<a href="<?php echo e(url('detailproduct/hapusComment/'.$Dataproduct->id_product.'/'.$key->id_comments)); ?>" style="color:red;">HAPUS</a>
																			<?php endif; ?>
																		<?php endif; ?>
																	</div>
																</div>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																<?php if($Datacomment->lastPage() > 1): ?>
																<div class="col-12">
			                            <div class="pagination_style fullwidth">
		                                <ul>
																			
																			<?php if($Datacomment->currentPage() != 1): ?>
																				<li>
																					<a href="<?php echo e($Datacomment->url(1)); ?>#komennya"><</a>
																				</li>
																			<?php endif; ?>
																			<?php for($i = 1; $i <= $Datacomment->lastPage(); $i++): ?>
																				<?php if($Datacomment->currentPage() == $i): ?>
																					<li class="current_number"><?php echo e($i); ?></li>
																				<?php else: ?>
																					<li><a href="<?php echo e($Datacomment->url($i)); ?>#komennya"><?php echo e($i); ?></a></li>
																				<?php endif; ?>
															        <?php endfor; ?>
																			<?php if($Datacomment->currentPage() != $Datacomment->lastPage()): ?>
																				<li><a href="<?php echo e($Datacomment->url($Datacomment->currentPage()+1)); ?>#komennya">></a></li>
																			<?php endif; ?>
		                                </ul>
			                            </div>
				                        </div>
																<?php endif; ?>
                              </div>
															
															<?php endif; ?>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--product info end-->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/layout_1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>