<?php $__env->startSection('content'); ?>
	<!-- customer login start -->
	<div class="customer_login">
		<div class="container">
			<div class="row">
				<!--login area start-->
				<div class="col-lg-6 col-md-6">
					<div class="account_form login">
						<form action="#">
							<h2>login and register</h2>
							<p>You can log in and register with one click. with a google account that is directly connected to the system</p>
							<a href="<?php echo e('auth/google'); ?>" title="Google+" class="col-md-12 btn btn-googleplus btn-lg">
								<i class="fa fa-google-plus fa-fw"></i> Login and Regis With Google
							</a>
						</form>
					</div>
				</div>
				<!--login area start-->
			</div>
		</div>
	</div>
	<!-- customer login end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/layout_1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>