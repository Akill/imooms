<?php

namespace App\Models\me;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use App\Models\from_tbl\tbl_wishlists;
use App\Models\from_tbl\tbl_ratings;

class MDetailproduct extends Model
{
	public function dataProduct($idProduct)
	{
		$data = DB::table('tbl_product')->where('id_product', $idProduct)->first();
		if ($data==null) {
			return abort(404);
		}
		return $data;
	}

	public function fotoProduct($idProduct)
	{
		$data = DB::table('tbl_foto_product')->where('id_product', $idProduct);
		return $data;
	}

	public function discount($idProduct)
	{
		$data = DB::table('tbl_discounts')
				->where('tbl_discounts.id_product', $idProduct)
				->join('tbl_product', 'tbl_discounts.id_product', '=', 'tbl_product.id_product')
				->having('tbl_discounts.min_limit','<',date('Y-m-d H:i:s'))
				->having('tbl_discounts.max_limit','>',date('Y-m-d H:i:s'))
				->first();
		if ($data==null) {
			return $data;
		}else {
			$disc = $data->harga * $data->jml_discount / 100;
			$hasil['hasil'] = $data->harga - $disc;
			$hasil['datadisc'] = $data;

			return $hasil;
		}
	}

	public function comment($idprod)
	{
		$data = DB::table('tbl_comments')
				->select(DB::raw('*,tbl_comments.created_at as created_at_cmt'))
				->join('users', 'tbl_comments.id', '=', 'users.id')
				->where('id_product', $idprod)
				->orderBy('tbl_comments.created_at', 'desc')
				->paginate(5);
		// dd($data);
		return $data;
	}

	public function wishlist($idproduct)
	{
		$hasil['valid'] = false;
		if (\Auth::user()) {
			if (tbl_wishlists::where('id',\Auth::user()->id)->where('id_product',$idproduct)->first()!=null) {
				$hasil['valid'] = true;
			}
		}
		$hasil['data'] = tbl_wishlists::where('id_product',$idproduct)->get();
		// dd(count($hasil['data']));
		return $hasil;
	}

	public function rating($idprod)
	{
		return tbl_ratings::where('id_product',$idprod)->first();
	}
}
