<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_product extends Model
{
	protected $table = 'tbl_product';
	protected $primaryKey = 'id_product';
}
