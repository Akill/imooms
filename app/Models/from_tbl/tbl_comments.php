<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_comments extends Model
{
	protected $table = 'tbl_comments';
  protected $primaryKey = 'id_comments';
}
