<?php

namespace App\Models\from_tbl;

use Illuminate\Database\Eloquent\Model;

class tbl_wishlists extends Model
{
	protected $table = 'tbl_wishlists';
	protected $primaryKey = 'id_wishlist';
}
