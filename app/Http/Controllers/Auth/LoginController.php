<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\User;
class LoginController extends Controller
{
  use AuthenticatesUsers;

  protected $redirectTo = '/home';

  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  public function redirectToProvider($provider)
  {
      return Socialite::driver($provider)->redirect();
  }

  public function handleProviderCallback($provider)
  {
      try {
        $socialuser = Socialite::driver($provider)->stateless()->user();
      } catch (\Exception $e) {
        return redirect('/');
      }
      // dd($socialuser);
      $id = '';
      $name = '';
      $email='';
      switch ($provider) {
        case 'google':
          $user = User::where('provider_id',$socialuser->id)->first();
          $id = $socialuser->id;
          $name = $socialuser->name;
          $email = $socialuser->email;
          break;
      }
      if (!$user){
        User::create([
          'provider' => $provider,
          'provider_id' => $id,
          'name'=>$name,
          'email'=>$email
        ]);
        $user = User::where('provider_id',$id)->first();
      }
      auth()->login($user, true);
      return redirect()->to('/');
  }
}
