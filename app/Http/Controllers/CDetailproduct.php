<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\me\MDetailproduct;
use Illuminate\Support\Facades\DB;
use App\tbl_comments;
use App\tbl_product;
use App\Models\from_tbl\tbl_wishlists;

class CDetailproduct extends Controller
{
	public function index($idproduct)
	{
		// dd($_SERVER["REMOTE_ADDR"]);
		$Detailproduct = new MDetailproduct;
		$data['Dataproduct'] = $Detailproduct->dataProduct($idproduct);
		$data['Datafotoproduct'] = $Detailproduct->fotoProduct($idproduct);
		$data['Datadiscount'] = $Detailproduct->discount($idproduct);
		$data['Datacomment'] = $Detailproduct->comment($idproduct);
		$data['Datawishlist'] = $Detailproduct->wishlist($idproduct);
		$data['Datarating'] = $Detailproduct->rating($idproduct);
		// dd(date('Y-m-d  H:i:s'));
		return view('VDetailproduct',$data);
	}

	public function simpanComment(Request $request)
	{
		\Auth::user() ? true : abort(404);
		$validatedData = $request->validate([
			'id_product' => 'required',
      'comment' => 'required|min:10|max:200',
    ]);
		tbl_product::findOrFail($request->id_product);
		// dd(\Auth::user()->id);
		$saveComment = new tbl_comments;
		$saveComment->id = \Auth::user()->id;
		$saveComment->id_product = $request->id_product;
		$saveComment->comment = $request->comment;
		$saveComment->save();
		// dd($saveComment->id_comments);
		\Session::flash('flash_message', 'NEW');
		\Session::flash('flash_id_comment', $saveComment->id_comments);
		return Redirect('detailproduct/'.$request->id_product.'#komennya');
	}

	public function hapusComment($idproduct,$idcomment)
	{
		\Auth::user() ? true : abort(404);
		$saveComment = tbl_comments::findOrFail($idcomment);
		$saveComment->delete();
		return Redirect('detailproduct/'.$idproduct.'#komennya');
	}

	public function wishlist($idproduct)
	{
		\Auth::user() ? true : abort(404);
		if (tbl_wishlists::where('id',\Auth::user()->id)->where('id_product',$idproduct)->first()==null) {
			$saveComment = new tbl_wishlists;
			$saveComment->id = \Auth::user()->id;
			$saveComment->id_product = $idproduct;
			$saveComment->save();
		}else {
			$saveComment = tbl_wishlists::where('id',\Auth::user()->id)->where('id_product',$idproduct);
			$saveComment->delete();
		}
		return Redirect('detailproduct/'.$idproduct);
	}
}
