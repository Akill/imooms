<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_discounts', function (Blueprint $table) {
            $table->increments('id_discounts');
            $table->string('id_product');
            $table->double('jml_discount', 20, 0);
            $table->dateTime('min_limit');
            $table->dateTime('max_limit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_discounts');
    }
}
