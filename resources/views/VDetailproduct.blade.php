@extends('layouts/layout_1')

@section('content')

	{{-- <!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
			<div class="container">
					<div class="breadcrumbs_inner">
							<div class="row">
									<div class="col-12">
											<div class="breadcrumb_content">
													<h3>product details</h3>
													<ul>
															<li><a href="index.html">home</a></li>
															<li><i class="fa fa-angle-right"></i></li>
															<li>product details</li>
													</ul>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--breadcrumbs area end--> --}}
	<br>
	<!--single product wrapper start-->
	<div class="single_product_wrapper">
			<div class="container">
					<div class="row">
							<div class="col-lg-5 col-md-6">
									<div class="product_gallery">
											<div class="tab-content produc_thumb_conatainer">
												@php
													$i=1;
												@endphp
												@foreach ($Datafotoproduct->get() as $key)
													@if ($key->status=='ACTIVE')
														<div class="tab-pane fade show active" id="{{$key->id_foto_product}}" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="{{url('products/600x600/'.$key->gambar)}}" alt=""></a>
															</div>
														</div>
													@else
														<div class="tab-pane fade" id="{{$key->id_foto_product}}" role="tabpanel" >
															<div class="modal_img">
																<a href="#"><img src="{{url('products/600x600/'.$key->gambar)}}" alt=""></a>
															</div>
														</div>
													@endif
												@endforeach
											</div>
											<div class="product_thumb_button">
													<ul class="nav product_d_button" role="tablist">
															@php
																$i=1;
															@endphp
															@foreach ($Datafotoproduct->get() as $key)
																@if ($key->status=='ACTIVE')
																<li >
																	<a class="active" data-toggle="tab" href="#{{$key->id_foto_product}}" role="tab" aria-controls="{{$key->id_foto_product}}" aria-selected="true"><img src="{{url('products/118x118/'.$key->gambar)}}" alt=""></a>
																</li>
																@else
																<li >
																	<a data-toggle="tab" href="#{{$key->id_foto_product}}" role="tab" aria-controls="{{$key->id_foto_product}}" aria-selected="false"><img src="{{url('products/118x118/'.$key->gambar)}}" alt=""></a>
																</li>
																@endif
															@endforeach

													</ul>
											</div>
									</div>
							</div>
							<div class="col-lg-7 col-md-6">
									<div class="product_details">
											<h3>{{$Dataproduct->nama_produk}}</h3>
											<div class="countdown_product_price">
												@if ($Datadiscount==null)
													<span class="current_price"> Rp. {{number_format($Dataproduct->harga,0)}}</span>
												@else
													<span class="current_price"> Rp. {{number_format($Datadiscount['hasil'],0)}}</span>
													<span class="old_price"> Rp. {{number_format($Dataproduct->harga,0)}}</span>

												@endif

											</div>
											<div class="product_ratting">
													<ul>
														@if ($Datarating==null)
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
															<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
														@else
															@for ($i=0; $i < 5; $i++)
																@if ($Datarating->rating>$i)
																	<li><a href="#"><i class="ion-ios-star"></i></a></li>
																@else
																	<li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
																@endif
															@endfor
														@endif
													</ul>
													<ul>
														<li><a href="#komennya">{{$Datacomment->total()}} Comments</a></li>
													</ul>
											</div>
										 <div class="product_description">
											 	@if ($Dataproduct->stock>0)
												 	<p>Sisa Produk : <label>{{$Dataproduct->stock}}</label></p>
												@else
													<p>Sisa Produk : <label style="color:red">{{$Dataproduct->stock}} maaf persediaan habis</label></p>
												@endif
												<p>{{$Dataproduct->deskripsi}}</p>
										 </div>
											<div class="product_details_action">
													<h3>Available Options</h3>
													@if ($Dataproduct->stock>0)
													<div class="product_stock">
															<label>Quantity</label>
															<input min="1" max="{{$Dataproduct->stock}}" type="number">
													</div>
													<div class="product_action_link">
															<ul>
																	<li class="product_cart"><a href="#" title="Add to Cart">Langsung beli</a></li>
																	<li class="product_cart"><a href="#" title="Add to Cart">Add to Cart</a></li>
																	@if (count($Datawishlist['data'])>0)
																		@if ($Datawishlist['valid'])
																			<li class="add_links"><a href="{{url('detailproduct/wishlist/'.$Dataproduct->id_product)}}" title="Add to Wishlist"><i class="ion-ios-heart"></i> {{count($Datawishlist['data'])}} wishlist</a></li>
																		@else
																			@if (\Auth::user())
																				<li class="add_links"><a href="{{url('detailproduct/wishlist/'.$Dataproduct->id_product)}}" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> {{count($Datawishlist['data'])}} wishlist</a></li>
																			@else
																				<li class="add_links"><a href="{{url('login')}}" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> {{count($Datawishlist['data'])}} wishlist</a></li>
																			@endif
																		@endif
																	@else
																		<li class="add_links"><a href="{{url('detailproduct/wishlist/'.$Dataproduct->id_product)}}" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> {{count($Datawishlist['data'])}} wishlist</a></li>
																	@endif
																	{{-- <li class="add_links"><a href="#" title="Add to Compare"><i class="ion-loop"></i> Add to compare</a></li> --}}
															</ul>
													</div>
													@else
													@endif
													<div class="social_sharing">
															<span>Share :</span>
															<ul>
																	{{-- <li><a href="#" class="bg-facebook" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i> Share</a></li>
																	<li><a href="#" class="bg-Tweet" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i> Tweet</a></li>
																	<li><a href="#" class="bg-pinterest" data-toggle="tooltip" title="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a></li> --}}
																	<li>
																		<a href="{{url('detailproduct/'.$Dataproduct->id_product)}}"
																		onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
																		class="bg-google" data-toggle="tooltip" title="google">
																			<i class="fa fa-google"> </i> Google
																		</a>
																	</li>
															</ul>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--single product wrapper end-->

	<!--product info start-->
	<div class="product_d_info">
			<div class="container">
					<div class="row">
							<div class="col-12">
									<div class="product_d_inner">
											<div class="product_info_button">
													<ul class="nav" role="tablist">
															<li >
																	<a data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">More info</a>
															</li>
															{{-- <li>
																	 <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Data sheet</a>
															</li> --}}
															<li>
																 <a class="active" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">comments</a>
															</li>
													</ul>
											</div>
											<div class="tab-content">
													<div class="tab-pane fade" id="info" role="tabpanel" >
															<div class="product_info_content">
																	<p>Tanggal Entry : <b>{{date('F d, Y', strtotime($Dataproduct->updated_at))}} at {{date('h:i a', strtotime($Dataproduct->updated_at))}}</b></p>
																	<br>
																	<p>{{$Dataproduct->more_info}}</p>
															</div>
													</div>
													<div class="tab-pane fade show active" id="reviews" role="tabpanel" >
															<div class="product_review_form">
																	{{ Form::open(array('url' => 'detailproduct/simpanComment')) }}
																		<h2>Add a comment </h2>
																		<p>Your email address will not be published. Required fields are marked </p>
																		@if (Route::has('login'))
                                      @auth
																				<div class="row">
																					<div class="col-12">
																						@if ($errors->any())
																							<label for="review_comment" style="color:red;">{{$errors->all()[0]}}</label>
																						@endif
																						<textarea name="comment" id="review_comment" >{{ old('comment') }}</textarea>
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label disabled for="author">Name</label>
																						<input disabled id="author" value="{{ Auth::user()->name }}" type="text">
																					</div>
																					<div class="col-lg-6 col-md-6">
																						<label for="email">Email </label>
																						<input disabled id="email" value="{{ Auth::user()->email }}" type="text">
																					</div>
																				</div>
																				<input hidden type="text" name="id_product" value="{{$Dataproduct->id_product}}">
																				{{ csrf_field() }}
																				<button type="submit">Submit</button>
                                      @else
																				<a href="{{url('auth/google')}}" title="Google+" class="col-md-12 btn btn-googleplus btn-lg">
																					<i class="fa fa-google fa-fw"></i> Login and Regis With Google
																				</a>
                                      @endauth
                                    @endif
																 {{ Form::close() }}
															</div>
															@if (count($Datacomment)>0)
															{{-- buka comments --}}
															<div class="comments_box">
                                <h3 id="komennya">{{count($Datacomment)}} - {{$Datacomment->total()}} comments	</h3>
																@foreach ($Datacomment as $key)
																<div class="comment_list">
																	<div class="comment_content">
																		<div class="comment_meta">
																			<div class="comment_title">
																				<h5><a href="#">{{$key->name}}</a>
																					@if ( Session::has('flash_message') )
																						@if ( Session::get('flash_id_comment')==$key->id_comments )
																							<label style="color:green; font-size:0.7em;">{{ Session::get('flash_message') }}</label>
																						@endif
																					@endif
																				</h5>
																				<span>{{date('F d, Y', strtotime($key->created_at_cmt))}} at {{date('h:i a', strtotime($key->created_at_cmt))}}</span>
																			</div>
																		</div>
																		<p>{{$key->comment}}</p>
																		@if (\Auth::user())
																			@if ($key->id==\Auth::user()->id)
																				<a href="{{url('detailproduct/hapusComment/'.$Dataproduct->id_product.'/'.$key->id_comments)}}" style="color:red;">HAPUS</a>
																			@endif
																		@endif
																	</div>
																</div>
																@endforeach
																@if ($Datacomment->lastPage() > 1)
																<div class="col-12">
			                            <div class="pagination_style fullwidth">
		                                <ul>
																			{{-- <li><a href="#"><</a></li> --}}
																			@if ($Datacomment->currentPage() != 1)
																				<li>
																					<a href="{{ $Datacomment->url(1) }}#komennya"><</a>
																				</li>
																			@endif
																			@for ($i = 1; $i <= $Datacomment->lastPage(); $i++)
																				@if ($Datacomment->currentPage() == $i)
																					<li class="current_number">{{ $i }}</li>
																				@else
																					<li><a href="{{ $Datacomment->url($i) }}#komennya">{{ $i }}</a></li>
																				@endif
															        @endfor
																			@if ($Datacomment->currentPage() != $Datacomment->lastPage())
																				<li><a href="{{ $Datacomment->url($Datacomment->currentPage()+1) }}#komennya">></a></li>
																			@endif
		                                </ul>
			                            </div>
				                        </div>
																@endif
                              </div>
															{{-- tutup comments --}}
															@endif
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>
	<!--product info end-->


@endsection
